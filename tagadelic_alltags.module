<?php

/**
 * @file
 * Generates a tagadelic block for all vocabularies
 */


/**
 * Implements hook_block_info().
 */
function tagadelic_alltags_block_info() {
  $blocks['tagadelic_alltags'] = array(
    'info' => t('Tagadelic all tags'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function tagadelic_alltags_block_view($delta) {
  $block = array();
  switch ($delta) {
    case 'tagadelic_alltags':
      $block['subject'] = t('All tags');
      $block['content'] = tagadelic_alltags_get_block();
      break;
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function tagadelic_alltags_block_configure($delta = '') {
  $form = array();
  if ($delta == 'tagadelic_alltags') {
    $form['tagadelic'] = array(
      '#type' => 'fieldset',
      '#title' => 'Tagadelic all tags settings',
    );
    $vocs = array();
    foreach (taxonomy_get_vocabularies(NULL) as $vocabulary) {
      $vocs[$vocabulary->vid] = $vocabulary->name;
    }
    $form['tagadelic']['tagadelic_alltags_vocabularies'] = array(
      '#title' => t('Vocabularies: '),
      '#type' => 'checkboxes',
      '#options' => $vocs,
      '#default_value' => variable_get('tagadelic_alltags_vocabularies', array()),
      '#description' => t('Leave all checkboxes unticked if you want to include all vocabularies.'),
    );
    $form['tagadelic']['tagadelic_alltags_level'] = array(
      '#type' => 'textfield',
      '#title' => t('Level'), 
      '#default_value' => variable_get('tagadelic_alltags_level', 6), 
    );
    $form['tagadelic']['tagadelic_alltags_amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Amount'),
      '#default_value' => variable_get('tagadelic_alltags_amount', 60),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function tagadelic_alltags_block_save($delta = '', $edit) {
  if ($delta == 'tagadelic_alltags') {
    variable_set('tagadelic_alltags_vocabularies', array_values(array_filter($edit['tagadelic_alltags_vocabularies'])));
    variable_set('tagadelic_alltags_level', $edit['tagadelic_alltags_level']);
    variable_set('tagadelic_alltags_amount', $edit['tagadelic_alltags_amount']);
  }
}

/**
 * Generates block content using the tagadelic module
 */ 
function tagadelic_alltags_get_block() {

  $vocs = variable_get('tagadelic_alltags_vocabularies', array());
  if (count($vocs) == 0) {
    foreach (taxonomy_get_vocabularies(NULL) as $vocabulary) {
      $vocs[] = $vocabulary->vid;
    }
  }

  $level = variable_get('tagadelic_alltags_level', 6);
  $amount = variable_get('tagadelic_alltags_amount', 60);

  $tags = tagadelic_get_weighted_tags($vocs, $level, $amount);
  $tags = tagadelic_sort_tags($tags);
  $output = theme('tagadelic_weighted', array('terms' => $tags));

  if (!$output) {
    return drupal_not_found();
  }

  $output = "<div class=\"wrapper tagadelic\">$output</div>";
  return $output;
}
