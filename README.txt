DESCRIPTION
-----------
Tagadelic all tags is a small block module that generates a unique tag 
cloud for all or for selected vocabularies. You must install Tagadelic 
in order to use Tagadelic all tags.
